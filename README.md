### WP Education Blog
Learning the basic skills of working with Wordpress.

### Dependencies
- Apache 2
- MySQL

### Clone your project
- Go to your computer's shell and type the following command with your SSH or HTTPS URL:
- `$ git clone git@gitlab.com:chaba3454/education-blog-wp.git`

### How to run website on your local machine
###
- Contact me to get the DATABASE: http://t.me/evgeniyochkalov
- Import DATABASE: `mysql -u root -p education_db < education_db.sql`
- Enter your root password.
###
- Log into mysql: `mysql -u root -p`
- Enter your root password
- Create a new user: `CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';`   <i>customize newuser and password</i>.
- Grant privileges to the new user: `GRANT ALL PRIVILEGES ON education_db . * TO 'newuser'@'localhost';`   where <b>education_db</b> is the name of the database.
- Flush privileges: `flush privileges`
- Show existing grants: `SHOW GRANTS FOR 'newuser'@'localhost';`
- TYPE: `exit;`
###
- Open your project folder in terminal.
- Copy over the sample configuration file to the filename wp-config:
  `cp /wordpress/wp-config-sample.php /wordpress/wp-config.php`
- In wp-config.php set your site variables:
 
```
define( 'DB_NAME', 'education_db' );

define( 'DB_USER', 'newuser' );

define( 'DB_PASSWORD', 'password' );
```

- Save and close the file when you are finished.
###
- Creating virtual host file:
`sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/education.conf`
- Open a new file in the editor with root privileges:
`sudo nano /etc/apache2/sites-available/education.conf`
- Put this into education.conf (Change Directory, DocumentRoot):

```
<VirtualHost *:80>
        ServerName education.localhost
        ServerAdmin webmaster@localhost
        DocumentRoot <directory to project>/education-blog-wp/wordpress
        <Directory <directopry to project>/education-blog-wp/wordpress>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```
###
- Use : `sudo a2ensite education.conf` to connect virtual host.
- Use : `systemctl reload apache2` to restart Apache

- Go to http://education.localhost 



