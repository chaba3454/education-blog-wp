#!/bin/bash
# This script downloads wp-cli and run search-replace inside container. Second command line argument for script is passed
# as second argument for search-replace command
# #!/bin/bash
# This script downloads wp-cli and run search-replace inside container. Second command line argument for script is passed
# as second argument for search-replace command
# For example:
# $ ./search-replace.sh redwerk.localhost

cd /var/www/html
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
php wp-cli.phar --allow-root search-replace //education.com //education.localhost
