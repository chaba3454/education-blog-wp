#!/bin/bash
# This script imports database dump from /redwerk-website/dbdump/dump.sql to the container
mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < /var/lib/dbdump/dump.sql