<?php get_header() ?>
<!-- Content
================================================== -->
<div class="content-outer">

    <div id="page-content" class="row">

        <div id="primary" class="eight columns">


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article class="post">

                    <div class="entry-header cf">

                        <h1><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h1>

                        <p class="post-meta">

                            <time class="date" datetime="2014-01-14T11:24"><?php the_time('F jS, Y'); ?></time>
                            /
                            <span class="categories">

                                    <?php the_tags( '', ' / '); ?>

                                </span>

                        </p>

                    </div>

                    <div class="post-thumb">

                        <?php the_post_thumbnail('post-thumb'); ?>

                    </div>

                    <div class="post-content">

                        <?php the_content(); ?>

                    </div>

                </article> <!-- post end -->
            <?php  endwhile; ?>

                <!-- Pagination -->
                <?php the_posts_pagination( $args ); ?>
            <?php endif; ?>

        </div> <!-- Primary End-->

        <div id="secondary" class="four columns end">

            <?php get_sidebar(); ?>

        </div> <!-- Secondary End-->

    </div>

</div> <!-- Content End-->

<!-- Tweets Section
================================================== -->
<section id="tweets">

    <div class="row">

        <div class="tweeter-icon align-center">

            <i class="fa fa-twitter"></i>

        </div>

        <ul id="twitter" class="align-center">

            <li><span>
                This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
                <a href="<?php the_permalink(); ?>">http://t.co/CGIrdxIlI3</a>
                </span>
                <b><a href="<?php the_permalink(); ?>"><?php the_time('F jS, Y'); ?></a></b>
            </li>
        </ul>

        <p class="align-center"><a href="<?php the_permalink(); ?>" class="button">Follow us</a></p>

    </div>

</section> <!-- Tweets Section End-->




<?php get_footer() ?>
