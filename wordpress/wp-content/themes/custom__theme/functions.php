<?php 

add_action( 'wp_enqueue_scripts', 'style_theme' );
add_action( 'wp_footer', 'scripts_theme' );
add_action( 'after_setup_theme', 'theme_register_nav_menu' );
add_action( 'init', 'create_taxonomy' );
add_action( 'wp_enqueue_scripts', 'my_scripts_add' );
add_action( 'init', 'register_post_types' );
add_action( 'widgets_init', 'register_widgets' );

    function register_widgets() {
        register_sidebar(array(
            'name' => 'Left Sidebar',
            'id' => "left_sidebar",
            'description' => 'DESCRIPTION',
            'class' => '',
            'before_widget' => '<div class="widget %2$s">',
            'after_widget' => "</div>\n",
            'before_title' => '<h5 class="widgettitle">',
            'after_title' => "</h5>\n",
        ));
    }

    function my_scripts_add(){
        if(is_page('time')){
            wp_enqueue_script("time", get_bloginfo( 'stylesheet_directory' ) . '/assets/js/time.js');
        }
    }

    function create_taxonomy(){
        register_taxonomy( 'Hack', [ 'education' ], [
            'label'                 => '', // определяется параметром $labels->name
            'labels'                => [
                'name'              => 'Hacks',
                'singular_name'     => 'HAck',
                'search_items'      => 'Search Hack',
                'all_items'         => 'All Hack',
                'view_item '        => 'ViewHack',
                'parent_item'       => 'ParentHack',
                'parent_item_colon' => 'ParentHack:',
                'edit_item'         => 'EditHack',
                'update_item'       => 'UpdateHack',
                'add_new_item'      => 'Add NewHack',
                'new_item_name'     => 'New Hack Name',
                'menu_name'         => 'Hack',
            ],
            'description'           => 'Hack the education', // описание таксономии
            'public'                => true,
            // 'publicly_queryable'    => null, // равен аргументу public
            // 'show_in_nav_menus'     => true, // равен аргументу public
            // 'show_ui'               => true, // равен аргументу public
            // 'show_in_menu'          => true, // равен аргументу show_ui
            // 'show_tagcloud'         => true, // равен аргументу show_ui
            // 'show_in_quick_edit'    => null, // равен аргументу show_ui
            'hierarchical'          => false,
    
            'rewrite'               => true,
            //'query_var'             => $taxonomy, // название параметра запроса
            'capabilities'          => array(),
            'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
            'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
            'show_in_rest'          => null, // добавить в REST API
            'rest_base'             => null, // $taxonomy
            // '_builtin'              => false,
            //'update_count_callback' => '_update_post_term_count',
        ] );
    }       

    add_filter( 'the_content', "test_content" );
    function test_content($content) {
        $content .= "Thanks for Reading!";
        return $content;
    }

    add_filter( 'document_title_separator', 'filter_function_name_2298' );
        function filter_function_name_2298( $sep ){
            $sep = ' // ';
            return $sep;
        }

    function theme_register_nav_menu() {
        register_nav_menu( 'top', 'Primary Menu' );
        register_nav_menu( 'bottom', 'Footer Menu' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails', array( 'post', 'education' ) );
        add_theme_support( 'post-formats', array( 'aside', 'video' ) );
        add_image_size( 'post-thumb', 1300, 500, true );
        add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
            function my_navigation_template( $template, $class ){
                return '
                    <nav class="navigation %1$s" role="navigation">
                        <div class="nav-links">%3$s</div>
                    </nav>    
                    ';
            }
            the_posts_pagination( array(
                'end_size' => 2,
            ) );
    }

    function register_post_types(){
        register_post_type('education', array(
            'label'  => null,
            'labels' => array(
                'name'               => 'Education', // основное название для типа записи
                'singular_name'      => 'Education', // название для одной записи этого типа
                'add_new'            => 'Add to education hack', // для добавления новой записи
                'add_new_item'       => 'Add to education hack', // заголовка у вновь создаваемой записи в админ-панели.
                'edit_item'          => 'Edit education hack', // для редактирования типа записи
                'new_item'           => 'New in education hack', // текст новой записи
                'view_item'          => 'View in education hack', // для просмотра записи этого типа.
                'search_items'       => 'Search in education hack', // для поиска по этим типам записи
                'not_found'          => 'Not found', // если в результате поиска ничего не было найдено
                'not_found_in_trash' => 'Not found in trash', // если не было найдено в корзине
                'parent_item_colon'  => '', // для родителей (у древовидных типов)
                'menu_name'          => 'Education', // название меню
            ),
            'description'         => '',
            'public'              => true,
            // 'publicly_queryable'  => null, // зависит от public
            // 'exclude_from_search' => null, // зависит от public
            // 'show_ui'             => null, // зависит от public
            // 'show_in_nav_menus'   => null, // зависит от public
            'show_in_menu'        => true, // показывать ли в меню адмнки
            // 'show_in_admin_bar'   => null, // зависит от show_in_menu
            'show_in_rest'        => true, // добавить в REST API. C WP 4.7
            'rest_base'           => null, // $post_type. C WP 4.7
            'menu_position'       => 4,
            'menu_icon'           => 'dashicons-admin-links',
            //'capability_type'   => 'post',
            //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
            //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
            'hierarchical'        => false,
            'supports'            => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
            'taxonomies'          => ["Hack"],
            'has_archive'         => false,
            'rewrite'             => true,
            'query_var'           => true,
        ) );
    }
    
    function style_theme() {
        wp_enqueue_style( 'style', get_stylesheet_uri() );
        wp_enqueue_style( 'style-default', get_template_directory_uri() . '/assets/css/default.css' );
        wp_enqueue_style( 'style-layout', get_template_directory_uri() . '/assets/css/layout.css' );
        wp_enqueue_style( 'style-media', get_template_directory_uri() . '/assets/css/media-queries.css' );
        wp_enqueue_style( 'style-fonts', get_template_directory_uri() . '/assets/css/fonts.css' );
    }

    function scripts_theme() {
        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js' );
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script('flexslider', get_template_directory_uri() . '/assets/js/jquery.flexslider.js', ['jquery'],
            null, true );
        wp_enqueue_script('doubletaplogo', get_template_directory_uri() . '/assets/js/doubletaptogo.js', ['jquery'],
            null, true );
        wp_enqueue_script('init', get_template_directory_uri() . '/assets/js/init.js', ['jquery'],
            null, true );
        wp_enqueue_script('modernizr', get_template_directory_uri() . '/assets/js/modernizr.js', null,
            null, false );
    }
